<?php
include_once("IModel.php");
include_once("Book.php");
require_once("TestDBProps.php");

/** The Model is the class holding data about a collection of books. 
 * @author Rune Hjelsvold
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */
class DBModel implements IModel
{        
    /**
      * The PDO object for interfacing the database
      *
      */
    protected $db = null;  
    
    /**
	 * @throws PDOException
     */
    public function __construct($db = null)  
    {  
	    if ($db) 
		{
			$this->db = $db;
		}
		else
		{
            $this->db = new PDO('mysql:dbname=assignment1;host='.TEST_DB_HOST,
                        				TEST_DB_USER, TEST_DB_PWD,
										array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
		}
    }
    
    /** Function returning the complete list of books in the collection. Books are
     * returned in order of id.
     * @return Book[] An array of book objects indexed and ordered by their id.
	 * @throws PDOException
     */
    public function getBookList()
    {
		$booklist = array();
		
		$sth = $this->db->prepare('SELECT * FROM book ORDER BY id');
		$sth->execute();
		$booklist = $sth->fetchAll(PDO::FETCH_OBJ);
		
        return $booklist;
    }
    
    /** Function retrieving information about a given book in the collection.
     * @param integer $id the id of the book to be retrieved
     * @return Book|null The book matching the $id exists in the collection; null otherwise.
	 * @throws PDOException
     */
    public function getBookById($id)
    {
		
		if(!is_numeric($id)){throw new Exception("ADADADADADDADA");}
		$book = null;
		try{
		$sth = $this->db->query("SELECT * FROM book WHERE id=$id");
		
		$sth->execute();
		
		$row=$sth->fetch(PDO::FETCH_ASSOC);
		if($row){
		$book = new Book($row['title'], $row['author'], $row['description'], $row['id']);
	
        return $book;
			}
			else return NULL;
		}
		catch(PDOException $e){
			$e->getMessage();
		}
	}
	
    
    /** Adds a new book to the collection.
     * @param $book Book The book to be added - the id of the book will be set after successful insertion.
	 * @throws PDOException
     */
    public function addBook($book)
    {
		if(($book->title && $book->author)){
			
			try{
		$sth = $this->db->prepare("INSERT INTO book(title, author, description) VALUES(:tit, :auth, :des)");
		
		$sth->bindValue(':tit', $book->title, PDO::PARAM_STR);
		$sth->bindValue(':auth', $book->author, PDO::PARAM_STR);
		$sth->bindValue(':des', $book->description, PDO::PARAM_STR);
		$sth->execute();
		$book->id=$this->db->lastInsertId();
		
			}
			catch(PDOException $e){
				$e->getMessage();
			}
		}
		else
			echo "Please enter Title and Author";
    }

    /** Modifies data related to a book in the collection.
     * @param $book Book The book data to be kept.
     * @todo Implement function using PDO and a real database.
     */
    public function modifyBook($book)
    {
		if(($book->title && $book->author)){
		$sth = $this->db->prepare("UPDATE book SET title=:tit, author=:auth, description=:des WHERE id=$book->id");
		
		$sth->bindValue(':tit', $book->title, PDO::PARAM_STR);
		$sth->bindValue(':auth', $book->author, PDO::PARAM_STR);
		$sth->bindValue(':des', $book->description, PDO::PARAM_STR);
		
		$sth->execute();
		}
    }

    /** Deletes data related to a book from the collection.
     * @param $id integer The id of the book that should be removed from the collection.
     */
    public function deleteBook($id)
    {
		$sth = $this->db->prepare("DELETE FROM book WHERE id=:id");
		$sth->bindValue(':id', $id, PDO::PARAM_STR);
		$sth->execute();
		
    }
	
}

?>